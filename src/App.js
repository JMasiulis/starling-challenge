import React from 'react';
import { SavingsContainer } from './features/savings';

export default function App() {
  return <SavingsContainer />;
}
