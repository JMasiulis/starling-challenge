import * as api from './LibSavings';

export const FETCHING_TRANSACTIONS_DONE = 'FETCHING_TRANSACTIONS_DONE';
export const FETCHING_ACCOUNT_INFO_DONE = 'FETCHING_ACCOUNT_INFO_DONE';
export const FETCHING_SAVINGS_INFO_DONE = 'FETCHING_SAVINGS_INFO_DONE';
export const STORE_SAVINGS_DONE = 'STORE_SAVINGS_DONE';

export function getInitialData() {
  return async dispatch => {
    dispatch({
      type: FETCHING_TRANSACTIONS_DONE,
      payload: await api.getTransactions()
    });

    const accountData = await api.getAccountInfo();

    dispatch({
      type: FETCHING_ACCOUNT_INFO_DONE,
      payload: accountData
    });

    if (accountData.data && accountData.data.accounts[0]) {
      dispatch({
        type: FETCHING_SAVINGS_INFO_DONE,
        payload: await api.getSavingsGoal(
          accountData.data.accounts[0].accountUid
        )
      });
    }
  };
}

export function storeSavings({ accountUid, savingsGoalUid, savingsAmount }) {
  const body = {
    amount: {
      currency: 'GBP',
      minorUnits: savingsAmount
    }
  };
  return async dispatch => {
    dispatch({
      type: STORE_SAVINGS_DONE,
      payload: await api.transferMoneyIntoGoal({
        accountUid,
        savingsGoalUid,
        body
      })
    });
  };
}
