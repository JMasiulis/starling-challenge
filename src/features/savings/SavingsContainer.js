import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import Savings from './Savings';
import { getInitialData, storeSavings } from './actions';

class SavingsContainer extends Component {
  componentDidMount() {
    this.props.dispatch(getInitialData());
  }

  handleStoreSavings = () => {
    const { accountUid, savingsGoalUid, savingsAmount } = this.props;
    this.props.dispatch(
      storeSavings({ accountUid, savingsGoalUid, savingsAmount })
    );
  };

  render() {
    const { savingsAmount } = this.props;

    return (
      <Savings
        savingsAmount={savingsAmount}
        onClickStoreSavings={this.handleStoreSavings}
      />
    );
  }
}

SavingsContainer.propTypes = {
  savingsAmount: PropTypes.number,
  savingsGoalUid: PropTypes.string,
  accountUid: PropTypes.string,
  dispatch: PropTypes.func.isRequired
};

SavingsContainer.defaultProps = {
  savingsAmount: null,
  savingsGoalUid: '',
  accountUid: ''
};

const mapStateToProps = state => ({
  savingsAmount: state.savings.savingsAmount,
  accountUid: state.savings.accountUid,
  savingsGoalUid: state.savings.savingsGoalUid
});

export default connect(mapStateToProps)(SavingsContainer);
