import React from 'react';
import PropTypes from 'prop-types';

export default function Savings(props) {
  if (!props.savingsAmount) {
    return null;
  }

  return (
    <div>
      <div>Savings detected from given transactions: {props.savingsAmount}</div>
      <button type="submit" onClick={props.onClickStoreSavings}>
        Store savings
      </button>
    </div>
  );
}

Savings.propTypes = {
  savingsAmount: PropTypes.number,
  onClickStoreSavings: PropTypes.func.isRequired
};

Savings.defaultProps = {
  savingsAmount: null
};
