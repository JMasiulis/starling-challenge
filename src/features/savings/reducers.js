import update from 'immutability-helper';
import { addWeeks, isBefore } from 'date-fns';
import {
  FETCHING_TRANSACTIONS_DONE,
  FETCHING_ACCOUNT_INFO_DONE,
  FETCHING_SAVINGS_INFO_DONE
} from './actions';

const initialState = {
  savingsAmount: null,
  accountUid: null,
  savingsGoalUid: null
};

function calculateSavings(payload) {
  const lastWeekDate = addWeeks(new Date(), -1);

  if (!payload || !payload._embedded || !payload._embedded.transactions) {
    return 0;
  }

  const savings = payload._embedded.transactions.reduce(
    (total, transaction) => {
      if (
        transaction.amount > 0 ||
        isBefore(transaction.created, lastWeekDate)
      ) {
        return total;
      }

      const positiveAmount = Math.abs(transaction.amount).toFixed(2);
      const ceilValue = Math.ceil(positiveAmount);
      return total + (ceilValue - positiveAmount);
    },
    0
  );

  return savings > 0 ? parseFloat(savings.toFixed(2)) : null;
}

export default function(state = initialState, { type, payload }) {
  switch (type) {
    case FETCHING_TRANSACTIONS_DONE:
      return update(state, {
        savingsAmount: { $set: calculateSavings(payload.data) }
      });
    case FETCHING_SAVINGS_INFO_DONE:
      return update(state, {
        savingsGoalUid: { $set: payload.data.savingsGoalList[0].savingsGoalUid }
      });
    case FETCHING_ACCOUNT_INFO_DONE:
      return update(state, {
        accountUid: { $set: payload.data.accounts[0].accountUid }
      });

    default:
      return state;
  }
}
