import uuid from 'uuid/v1';
import http from '../../core/http';

export async function getTransactions() {
  const data = await http.get('/v1/transactions', { crossdomain: true });
  return data;
}

export async function getSavingsGoal(accountUid) {
  const data = await http.get(`/v2/account/${accountUid}/savings-goals`, {
    crossdomain: true
  });
  return data;
}

export async function transferMoneyIntoGoal({
  accountUid,
  savingsGoalUid,
  body
}) {
  const data = await http.put(
    `/v2/account/${accountUid}/savings-goals/${savingsGoalUid}/add-money/${uuid()}`,
    body,
    { crossdomain: true }
  );
  return data;
}

export async function getAccountInfo() {
  const data = await http.get('/v2/accounts');
  return data;
}
