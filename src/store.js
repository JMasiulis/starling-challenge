import { createStore, combineReducers, applyMiddleware } from 'redux';
import thunk from 'redux-thunk';
import savingsReducer from './features/savings/reducers';

const reducers = combineReducers({
  savings: savingsReducer
});

export default createStore(reducers, applyMiddleware(thunk));
