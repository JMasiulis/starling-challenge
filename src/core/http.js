import axios from 'axios';
import parseJson from './utils/parseJson';

const http = axios.create({
  mode: 'no-cors',
  headers: {
    Authorization:
      'Bearer mjkNWpRZtBWhCKBqUF0CxLKGBWqhj628L76zhZpxyoBmeigitm6jJZjqkLWe4XqB',
    Accept: 'application/json',
    'Content-Type': 'text/plain;charset=utf-8'
  },
  transformResponse: [parseJson]
});

export default {
  ...http
};
