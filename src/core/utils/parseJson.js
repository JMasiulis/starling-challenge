export default function parseJson(data) {
  if (typeof data === 'string') {
    try {
      return JSON.parse(data);
    } catch (error) {
      // Ignore
    }
  }

  return data;
}
